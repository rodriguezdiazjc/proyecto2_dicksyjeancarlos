/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import datos.BaseDatos;
import java.awt.Panel;
import java.io.IOException;
import java.sql.Date;
import java.util.Calendar;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author User
 */
public class Usuario {

    private BaseDatos oBD;

    public Usuario() {
        this.oBD = new BaseDatos();
    }

    public int validarLogin(String pCedula, String pContrasena) {
        int valor = 0;
        if (this.oBD.buscarContrasena(pCedula, pContrasena) == 1) {
            valor = 2;
        } else if (this.oBD.buscarCedula(pCedula) == 1) {
            valor = 1;
        }
        return valor;
    }

    public boolean enviarCorreo(String pCorreoDestino, int pIdUsuario, String pNombreUsuario, Mascota oMascota, boolean pIsAceptada) throws MessagingException, IOException {
        boolean correoEnviado = false;
        Properties propiedad = new Properties();
        propiedad.setProperty("mail.smtp.host", "smtp.gmail.com");
        propiedad.setProperty("mail.smtp.starttls.enable", "true");
        propiedad.setProperty("mail.smtp.port", "587");
        propiedad.setProperty("mail smtp auth", "true");
        Session sesion = Session.getDefaultInstance(propiedad);
        sesion.getProperties().put("mail.smtp.ssl.trust", "smtp.gmail.com");
        String correoEnvia = "albergue.lamascoteria@gmail.com";
        String contrasena = "programacion2";
        String receptor = pCorreoDestino;
        String asunto = "";
        String mensaje = "";
        java.sql.Date fechaActual = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        boolean conImagen = false;
        if (pIsAceptada) {
            asunto = "Solicitud aprobada";
            mensaje = "La administración le indica que su solicitud fue aceptada\n"
                    + "\nEl resumen de la adopción es el siguiente:\n"
                    + "Cédula: " + pIdUsuario + ".\n"
                    + "Nombre: " + pNombreUsuario + ".\n"
                    + "Características de la mascota:\n"
                    + "Nombre: " + oMascota.getNombre() + "\n"
                    + "Sexo: " + oMascota.getSexo() + "\n"
                    + "Edad: " + oMascota.getEdad() + "\n"
                    + "Tamaño: " + oMascota.getTamano() + "\n"
                    + "Color: " + oMascota.getColor() + "\n"
                    + "Fecha de ingreso: " + oMascota.getFecha_ingreso().toString() + "\n"
                    + "Fecha de adopción: " + fechaActual.toString() + "\n";
            if (oMascota.getFoto() != null) {
                Foto oFoto = new Foto();
                oFoto.cargarImagenTemporal(oMascota.getFoto());
                conImagen = true;                
            }
        } else {
            asunto = "Solicitud rechazada";
            mensaje = "Por disposiciones de la administración, se le indica que su solicitud fue rechazada.";
        }
        MimeMessage mail = new MimeMessage(sesion);
        try {
            mail.setFrom(new InternetAddress(correoEnvia));
            mail.addRecipient(Message.RecipientType.TO, new InternetAddress(receptor));
            mail.setSubject(asunto);
            BodyPart textoCorreo = new MimeBodyPart();
            textoCorreo.setText(mensaje);
            MimeMultipart multiParte = new MimeMultipart();
            multiParte.addBodyPart(textoCorreo);
            if (conImagen) {
                BodyPart imagenCorreo = new MimeBodyPart();
                imagenCorreo.setDataHandler(new DataHandler(new FileDataSource("C://Users//User//Documents//NetBeansProjects//ProyectoII//temp//temporal.jpg")));
                imagenCorreo.setFileName(oMascota.getNombre());
                multiParte.addBodyPart(imagenCorreo);
            }
            mail.setContent(multiParte);
            Transport transportar = sesion.getTransport("smtp");
            transportar.connect(correoEnvia, contrasena);
            transportar.sendMessage(mail, mail.getRecipients(Message.RecipientType.TO));
            transportar.close();
            correoEnviado = true;
        } catch (AddressException ex) {
            Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
            String prueba = ex.getMessage();
            correoEnviado = false;
        } catch (MessagingException ex) {
            Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
            correoEnviado = false;
        }
        return correoEnviado;
    }
    
    public char tipoUsuario(String pCedula) {
        return this.oBD.buscarTipo(pCedula);
    }
    
    public boolean registrarUsuario(int pCedula, String pContrasena, String pNombre, String pCorreo) {
        return this.oBD.registrarUsuario(pCedula, pContrasena, pNombre, pCorreo, 'C');
    }
    
    public String getNombre(String pCedula) {
        return this.oBD.nombreUsuario(pCedula);
    }
    
    public String getCorreo(String pCedula) {
        return this.oBD.correoUsuario(pCedula);
    }
    
}
